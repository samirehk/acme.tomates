var React = require('react');
var {Link, IndexLink} = require('react-router');
var authentication = require('authentication');

var Nav = React.createClass({
  render: function() {
    function renderLoginOrLogout() {
      if (authentication.isUserLogged()) {
        return (
          <ul className="menu">
            <li>
              Olá Samir
            </li>
            <li>
              <Link to="/logout" activeClassName="active" activeStyle={{
                fontWeight: 'bold'
              }}>Logout</Link>
            </li>
          </ul>
        )
      } else {
        return (
          <ul className="menu">
            <li>
              <Link to="/login" activeClassName="active" activeStyle={{
                fontWeight: 'bold'
              }}>Login</Link>
            </li>
            <li>
              <Link to="/cadastro" activeClassName="active" activeStyle={{
                fontWeight: 'bold'
              }}>Cadastro</Link>
            </li>
          </ul>
        )
      }
    }

    return (
      <div className="top-bar">
        <div className="top-bar-left">
          <ul className="menu">
            <li className="menu-text">
              ACME Tomates
            </li>
            <li>
              <IndexLink to="/" activeClassName="active" activeStyle={{
                fontWeight: 'bold'
              }}>Home</IndexLink>
            </li>

          </ul>
        </div>
        <div className="top-bar-right">
          {renderLoginOrLogout()}
        </div>
      </div>
    )
  }
});
module.exports = Nav
