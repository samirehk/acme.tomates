var React = require('react')

var CadastroForm = React.createClass({
  onFormSubmit: function(e){
    e.preventDefault();

    var email = this.refs.email.value;
    var password = this.refs.password.value;
    var name = this.refs.name.value;
    var cpf = this.refs.cpf.value;

    if (email.length > 0 && password.length > 0) {
        this.props.onRegister(email, password, name, cpf);
    }

  },
  render: function() {
    return (
      <div>
          <form onSubmit={this.onFormSubmit}>
              <div>
                  <input type="text" ref="email" placeholder="Email"></input>
              </div>
              <div>
                  <input type="password" ref="password" placeholder="Password"></input>
              </div>
              <div>
                  <input type="text" ref="name" placeholder="Name"></input>
              </div>
              <div>
                  <input type="text" ref="cpf" placeholder="Cpf"></input>
              </div>
              <div>
                  <button className="button expanded hollow">Cadastrar</button>
              </div>
          </form>
      </div>
    );
  }
})

module.exports = CadastroForm;
