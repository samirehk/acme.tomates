var React = require('react');

var LoginForm = React.createClass({
    onFormSubmit: function(e) {
        e.preventDefault();

        var email = this.refs.email.value;
        var password = this.refs.password.value;

        if (email.length > 0 && password.length > 0) {
            this.props.onLogin(email, password);
        }
    },
    render: function() {
        return (
            <div>
                <form onSubmit={this.onFormSubmit}>
                    <div>
                        <input type="text" ref="email" placeholder="Email"></input>
                    </div>
                    <div>
                        <input type="password" ref="password" placeholder="Password"></input>
                    </div>
                    <div>
                        <button className="button expanded hollow">Login</button>
                    </div>
                </form>
            </div>
        );
    }
});

module.exports = LoginForm;
