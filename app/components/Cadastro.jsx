var React = require('react');
var CadastroForm = require('CadastroForm')
var authentication = require('authentication')
var {Link, IndexLink} = require('react-router');

var Cadastro = React.createClass({
  getInitialState: function() {
    return {isLoading: false, success: undefined, errorMessage: undefined}
  },
  handleRegister: function(email, password, name, cpf) {
    //TODO implementar cadastro
    if (email.length === 0) {
      let emailValidationError = 'Email inválido'
      this.setState({isLoading: false, errorMessage: emailValidationError});
    } else if (password.length < 6) {
      let passwordValidationError = 'Senha muito pequena'
      this.setState({isLoading: false, errorMessage: passwordValidationError});
    } else {
      this.setState({isLoading: true})
      authentication.handleSignUp(email, password, name, cpf).then(() => {
        this.setState({isLoading: false, success: true})
      }, (e) => {
        this.setState({isLoading: false, errorMessage: e.message});
      })
    }

    //
  },
  render: function() {
    var {isLoading, errorMessage, success} = this.state;
    var that = this;
    function renderReturnMessage() {
      if (success) {
        return (
          <div className="text-center">
            <h3>Cadastro Realizado com sucesso!</h3>
            <p>Verifique sua caixa de email.</p>
            <Link to="/login">Login</Link>
          </div>
        )
      }
    }

    function renderRegistrationForm() {
      function renderError() {
        if (isLoading) {
          return <p className="text-center">Carregando...</p>
        } else if (typeof errorMessage === 'string') {
          return <p className="text-center">{errorMessage}</p>
        }
      }

      if (success !== true) {
        return (
          <div>
            <h1 className="text-center page-title">Cadastre-se</h1>
            {renderError()}
            <CadastroForm onRegister={that.handleRegister}/>
          </div>
        )
      }
    }

    return (
      <div>
        {renderReturnMessage()}

        {renderRegistrationForm()}
      </div>
    )
  }
})

module.exports = Cadastro
