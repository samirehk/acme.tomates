var React = require('react');
var {Link} = require('react-router');

var Home = (props) => {
    return (
        <div>
            <img src="img/tomato.webp" />
            <h1 className="text-center page-title">Bem Vindo à Acme Tomates</h1>
            <p>Temos prazer em produzir molhos de tomates artesanais com gosto da comida caseira!</p>
        </div>
    );
};
module.exports = Home
