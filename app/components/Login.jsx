var React = require('react');
var LoginForm = require('LoginForm')
var authentication = require('authentication')

var Login = React.createClass({
  getInitialState: function() {
    return {isLoading: false, errorMessage: undefined}
  },
  componentDidMount: function() {
    if(this.props.location.pathname == "/logout") {
      this.handleLogout()
      window.location.hash = '#/login'
    }
  },
  handleLogout: function() {
    authentication.logout()
  },
  handleLogin: function(email, password) {
    this.setState({isLoading: true})

    authentication.login(email, password)
    .then(() => {
      this.setState({isLoading: false, errorMessage: undefined})
      window.location.hash = '#/';
    },
    (e) => {
      this.setState({isLoading: false, errorMessage: e.message});
    })
  },
  render: function() {
    var {isLoading, errorMessage} = this.state;
    function renderReturnMessage() {
      if (isLoading) {
        return <p className="text-center">Carregando...</p>
      } else if (typeof errorMessage === 'string') {
        return <p className="text-center">{errorMessage}</p>
      }
    }

    return (
      <div>
        <h1 className="text-center page-title">Login</h1>
        {renderReturnMessage()}
        <LoginForm onLogin={this.handleLogin}/>
      </div>
    )
  }
})

module.exports = Login
