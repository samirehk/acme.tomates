var firebase = require("firebase/app");

module.exports = {
  handleSignUp: (email, password, name, cpf) => {
    return firebase.auth().createUserWithEmailAndPassword(email, password).then(() => {
      var user = firebase.auth().currentUser;

      firebase.database().ref('users/' + user.uid).set({name: name, email: email, cpf: cpf}).then(() => {
        firebase.auth().currentUser.sendEmailVerification()
      });
    })
  },
  login: (email, password) => {
    return firebase.auth().signInWithEmailAndPassword(email, password)
  },
  logout: () => {
    if (firebase.auth().currentUser) {
      firebase.auth().signOut();
    }
  },
  passwordReset: (email) => {
    return firebase.auth().sendPasswordResetEmail(email)
  },
  isUserLogged: () => {
    if (firebase.auth().currentUser)
      return true;
    else
      return false;
    }
}
