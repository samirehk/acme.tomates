// React Components
var React = require('react');
var ReactDOM = require('react-dom');
var {Route, Router, IndexRoute, hashHistory} = require('react-router');

// Firebase Comonents
var firebase = require("firebase/app");
require("firebase/auth");
require("firebase/database");

// App Components
var Main = require('Main');
var Login = require('Login');
var Cadastro = require('Cadastro');
var Home = require('Home');

// Load Foundation
require('style!css!foundation-sites/dist/foundation.min.css');
$(document).foundation();

//App CSS
require('style!css!sass!applicationStyle');

//Firebase Initialization
var config = {
  apiKey: "AIzaSyA1Pqh7DXelMLD_SYhfg5OYLJ3Nc2p0NYQ",
  authDomain: "tomate-acme.firebaseapp.com",
  databaseURL: "https://tomate-acme.firebaseio.com",
  projectId: "tomate-acme",
  storageBucket: "",
  messagingSenderId: "244987517734"
};

firebase.initializeApp(config);

ReactDOM.render(
  <Router history={hashHistory}>
  <Route path="/" component={Main}>
    <Route path="cadastro" component={Cadastro}/>
    <Route path="login" component={Login}/>
    <Route path="logout" component={Login}/>
    <IndexRoute component={Home}/>
  </Route>
</Router>, document.getElementById('app'));
